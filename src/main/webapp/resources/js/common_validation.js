function validateFieldsOnFormSubmit(){
	var flag=0;
	$('input[required]').each(function(){
		$(this).css('border','1px solid #bfc1d1');
		if($(this).val()==''&&flag==0){
			flag=1;
			$(this).focus().css('border','2px solid red');
			//alert($(this).attr('placeholder'))
		}
	});
	$('select[required]').each(function(){
		$(this).css('border','1px solid #bfc1d1');
		if($(this).val()==''&&flag==0){
			flag=1;
			$(this).css('border','2px solid red');
			//alert($(this).attr('placeholder'))
		}

	});
	$('radio[required]').each(function(){
		$(this).css('border','1px solid #bfc1d1');
		if($(this).val()==''&&flag==0){
			flag=1;
			$(this).css('border','2px solid red');
			//alert($(this).attr('placeholder'))
		}

	});
	if(flag==0){
		return true;
	}
	return false;
}