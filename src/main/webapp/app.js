﻿var attendanceURL='';
//var attendanceURL='http://139.162.58.102:8080/BMA/';


//Do below Signed APK 
//i chages for app version
//1. cordova config file
//2.app.js
//3. db table

//ii create account button in login.html page comment it
(function () {
    'use strict';
    window.localStorage.setItem("appVersion",'0.0.6');
    
    angular.module('UserModule', []);
    angular.module('GlobalModule', []);
   
    
    angular.module('app', [
                           'ngRoute',
                           'ngCookies',
                           'UserModule',
                           'GlobalModule'])
        .config(config)
        .run(run);
    
   
     
    config.$inject = ['$routeProvider', '$locationProvider'];
    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/login', {
                templateUrl: 'modules/user/partials/login.html',
            }).when('/register', {
                templateUrl: 'modules/user/partials/register.html',
            }).otherwise({
            	redirectTo: '/login' 
            });
    }
   
    run.$inject = ['$rootScope', '$location', '$cookieStore', '$http'];
    function run($rootScope, $location, $cookieStore, $http) {
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            // redirect to login page if not logged in and trying to access a restricted page
           // var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;  // need jquery
           /* var loggedIn = $rootScope.globals.currentUser;
            if ( !loggedIn) {
                $location.path('/login');
            }*/
        });
    }

})();