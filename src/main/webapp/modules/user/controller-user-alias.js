'use strict';

var controllers = angular.module('UserModule');


controllers.controller('UserAliasCtrl' , ['$scope','$location','$http','$rootScope','UserModule_loginService','GlobalModule_dataStoreService', function ($scope, $location, $http,$rootScope, UserModule_loginService,GlobalModule_dataStoreService) {
	$rootScope.userDetails=GlobalModule_dataStoreService.loadData('UserModule','userDetails');
	$scope.id=GlobalModule_dataStoreService.loadData('UserModule','id');
	console.log("$rootScope.userDetails"+JSON.stringify($rootScope.userDetails));
	$scope.fetchUserAlias = function() {
		UserModule_loginService.fetchUserAlias().then(function(response){
			//Success
			$scope.userAlias = response.data;
			console.log($scope.userAlias);
		},function(response){
			//Error			
			
		});
	};
	$scope.fetchUserAlias();
	
	$scope.fetchUserAliasWhenSkip = function() {
		if(!$rootScope.userDetails.organizationAlias){
			$rootScope.userDetails.organizationAlias="Organization";
		}
		if(!$rootScope.userDetails.batchAlias){
			$rootScope.userDetails.batchAlias="Batch";
		}
		if(!$rootScope.userDetails.managerAlias){
			$rootScope.userDetails.managerAlias="Manager";
		}
		
		location.href = '#/attendence';
	};
	
	$scope.addUserAlias = function(alias) {
		
		for(var i=0;i<alias.length;i++){
			alias[i].userId = $rootScope.userDetails.id;
		}
		$scope.aliasObj = {userAliasList:alias};
		
		console.log("ALIES OBJ\n"+JSON.stringify($scope.aliasObj));
		UserModule_loginService.addUserAlias($scope.aliasObj).then(function(response){
			//Success
			
			UserModule_loginService.showalias($rootScope.userDetails.id).then(function(response){
				//Success
				//$rootScope.id=id;
				//alert($rootScope.userDetails.id);
				$scope.currentUser=response.data;
				$rootScope.userDetails.organizationAlias=$scope.currentUser.organizationAlias;
				$rootScope.userDetails.batchAlias=$scope.currentUser.batchAlias;
				$rootScope.userDetails.managerAlias=$scope.currentUser.managernAlias;
				if(!$rootScope.userDetails.organizationAlias){
					$rootScope.userDetails.organizationAlias="Organization";
				}
				if(!$rootScope.userDetails.batchAlias){
					$rootScope.userDetails.batchAlias="Batch";
				}
				if(!$rootScope.userDetails.managerAlias){
					$rootScope.userDetails.managerAlias="Manager";
				}
				
				console.log("userDetails\n"+JSON.stringify($rootScope.userDetails));
		  		GlobalModule_dataStoreService.storeData('UserModule','userDetails',$rootScope.userDetails);
		  		UserModule_loginService.fetchMenu($rootScope.userDetails.roleObj.id).then(function(response){
		  			$scope.menuList=response.data;
		  			GlobalModule_dataStoreService.storeData('UserModule','menuList',$scope.menuList);
		  			console.log("$scope.menuList \n"+JSON.stringify($scope.menuList));
		  			location.href = '#/attendence';
					
		  		},function(response){
					
				});
		  		$(".loader").fadeOut('slow');
		  		//GlobalModule_notificationService.notification("success","UserModule","Login Successfully");
		  		
		
			},function(response){
				//Error			
				$(".loader").fadeOut('slow');
				GlobalModule_notificationService.notification("success","UserModule","Login Failed");
			});
			/*location.href = '#/attendence';
			console.log($scope.response);*/
		},function(response){
			//Error			
			
		});
	};
	
}]);
