'use strict';

var controllers = angular.module('UserModule');


controllers.controller('MenuCtrl' , ['$scope','$location','$http','$rootScope','UserModule_loginService','GlobalModule_dataStoreService','GlobalModule_notificationService', function ($scope, $location, $http,$rootScope, UserModule_loginService,GlobalModule_dataStoreService,GlobalModule_notificationService) {
	$rootScope.userDetails=GlobalModule_dataStoreService.loadData('UserModule','userDetails');
	//$scope.myprofileflag=GlobalModule_dataStoreService.loadData('AttendenceModule','myprofileflag');
			$scope.myProfile = function() {
				$scope.userObj={};
				$scope.userObj.myprofileflag=false;
				//GlobalModule_dataStoreService.storeData('AttendenceModule','myprofileflag',true);
				//console.log($scope.myprofileflag);
				console.log("myProfile");
				$scope.member =$rootScope.userDetails;
				$scope.member.phoneNo= parseInt ($scope.member.phoneNo,10);
				$scope.member.selectedRole =$scope.member.roleObj.id;
				 //GlobalModule_dataStoreService.storeData('AttendenceModule','myprofile',$scope.userObj.flag);
				 console.log($scope.member);
				 GlobalModule_dataStoreService.storeData('AttendenceModule','profileflag',false);
				GlobalModule_dataStoreService.storeData('AttendenceModule','selectedMember',$scope.member);
				location.href = '#/updateMember';
			};
			
			
			$scope.logout = function(){
				$(".loader").show();
				 localStorage.removeItem("userDetails");
				 localStorage.removeItem("userId");
				 localStorage.removeItem("username");
				 localStorage.removeItem("userloggedIn");
				 localStorage.removeItem("menuList");
				 localStorage.removeItem("userpin");
				 localStorage.clear();
				 
				 GlobalModule_dataStoreService.deleteDatastore();
				 /*$rootScope.userDetails=undefined;
				 GlobalModule_dataStoreService.storeData('UserModule','userDetails',undefined);
				 GlobalModule_dataStoreService.storeData('AttendenceModule','managerName',undefined);
				 GlobalModule_dataStoreService.storeData('AttendenceModule','orgId',undefined);
				 GlobalModule_dataStoreService.storeData('AttendenceModule','batchId',undefined);
				 GlobalModule_dataStoreService.storeData('AttendenceModule','batchName',undefined);
				 GlobalModule_dataStoreService.storeData('AttendenceModule','dateAttn',undefined);
				 GlobalModule_dataStoreService.storeData('UserModule','userId',undefined);
				 GlobalModule_dataStoreService.storeData('UserModule','userDetails',undefined);
				 GlobalModule_dataStoreService.storeData('UserModule','loggedIn',undefined);
				 GlobalModule_dataStoreService.storeData('UserModule','menuList',undefined);*/
				
				 
				 $location.path('/login');
				 $(".loader").fadeOut('slow');
			};
			
}]);
