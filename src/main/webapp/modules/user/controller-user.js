'use strict';

var controllers = angular.module('UserModule');


controllers.controller('LoginCtrl' , ['$scope','$location','$http','$rootScope','UserModule_loginService','GlobalModule_dataStoreService','GlobalModule_notificationService', function ($scope, $location, $http,$rootScope, UserModule_loginService,GlobalModule_dataStoreService,GlobalModule_notificationService) {
	$rootScope.userDetails=GlobalModule_dataStoreService.loadData('UserModule','userDetails');
	
	
	$scope.menuList={};
	$scope.loginUser = function(user, remember) {
		$scope.rememberFlag = remember; 
		//alert($scope.rememberFlag);
		$(".loader").show();
		console.log(user);
		UserModule_loginService.loginUser(user).then(function(response){
			//Success
			$scope.currentUser=response.data;
			$rootScope.userDetails=$scope.currentUser;
			console.log($rootScope.userDetails.roleObj);
			/*if($rootScope.userDetails.roleObj.id!=3 ){*/
				if(!$rootScope.userDetails.organizationAlias){
					$rootScope.userDetails.organizationAlias="Organization";
				}
				if(!$rootScope.userDetails.batchAlias){
					$rootScope.userDetails.batchAlias="Batch";
				}
				if(!$rootScope.userDetails.managerAlias){
					$rootScope.userDetails.managerAlias="Manager";
				}
				
		//	}
		
			console.log("userDetails\n"+JSON.stringify($rootScope.userDetails));
	  		GlobalModule_dataStoreService.storeData('UserModule','userDetails',$rootScope.userDetails);
	  		
	  		//for auto login store uname and pass to local storage
	  		 window.localStorage.setItem("userDetails", JSON.stringify($rootScope.userDetails));
	  		 window.localStorage.setItem("userId", $rootScope.userDetails.id);
	  		 window.localStorage.setItem("username", $rootScope.userDetails.userName);
	  		 window.localStorage.setItem("userloggedIn",true);
	  	 	
	  		 
	  		 
	  		//fetch  menu
	  		UserModule_loginService.fetchMenu($rootScope.userDetails.roleObj.id).then(function(response){
			  		$scope.menuList=response.data;
			  		GlobalModule_dataStoreService.storeData('UserModule','menuList',$scope.menuList);
			  		console.log("$scope.menuList \n"+JSON.stringify($scope.menuList));
			  		
			  		 window.localStorage.setItem("menuList", JSON.stringify($scope.menuList));
			  		 
			  		 if($scope.rememberFlag == true){
			  			//alert("in iiff");
			  			 $location.path("/pingeneration");
			  			
			  		 }
			  		 else{
			  			//alert("in ellssee");
			  			$scope.checkLogin();
			  		 }
			  		 //$scope.checkLogin();
			  		//location.href = '#/attendence';
						
	  		},function(response){
				
			});
	  		
	  		
	  		
	  		$(".loader").fadeOut('slow');
	  		//GlobalModule_notificationService.notification("success","UserModule","Login Successfully");
	  		
	
		},function(response){
			//Error			
			$(".loader").fadeOut('slow');
			GlobalModule_notificationService.notification("success","UserModule","Invalid Login Credentials");
		});
	};


	/*$rootScope.mynetworkStatus=undefined;
	function checkConnection() {
	    $scope.networkState = navigator.connection.type;

	    $scope.states = {};
	    $scope.states[Connection.UNKNOWN]  = 'Unknown connection';
	    $scope.states[Connection.ETHERNET] = 'Ethernet connection';
	    $scope.states[Connection.WIFI]     = 'WiFi connection';
	    $scope.states[Connection.CELL_2G]  = 'Cell 2G connection';
	    $scope.states[Connection.CELL_3G]  = 'Cell 3G connection';
	    $scope.states[Connection.CELL_4G]  = 'Cell 4G connection';
	    $scope.states[Connection.CELL]     = 'Cell generic connection';
	    $scope.states[Connection.NONE]     = 'No network connection';

	  
	    if($scope.states[$scope.networkState] ==  $scope.states[Connection.NONE]){
	    	$rootScope.mynetworkStatus=true;
	    }
	}

	checkConnection();*/
	
	
	
	
	$scope.checkLogin =function(){
		
		 $(".loader").show();
		 
		 $(".loader").fadeOut('slow');
		 
		/* if( $rootScope.mynetworkStatus==true){
			 GlobalModule_notificationService.notification("success","UserModule","Please check network connection");
		 }
		 else{*/
		 $scope.storedDetails = JSON.parse(localStorage.getItem("userDetails"));
		 $scope.userid = localStorage.getItem("userid");
	
		 var loggedIn = window.localStorage.getItem("userloggedIn");
	     var userId = window.localStorage.getItem("userId");
	     var userDetails = window.localStorage.getItem("userDetails");
	     var username = window.localStorage.getItem("username");
	     var menuList = window.localStorage.getItem("menuList");
	     var userpin = window.localStorage.getItem("userpin");
	     
	     if(userpin !=null)
			{
	    	 $scope.userDetails = JSON.parse(userDetails);
				/*if($scope.userDetails.roleObj.id!=3 ){*/
					if(!$scope.userDetails.organizationAlias){
						$rootScope.userDetails.organizationAlias="Organization";
						$scope.userDetails.organizationAlias="Organization";
					}
					if(!$scope.userDetails.batchAlias){
						$rootScope.userDetails.batchAlias="Batch";
						$scope.userDetails.batchAlias="Batch";
					}
					if(!$scope.userDetails.managerAlias){
						$rootScope.userDetails.managerAlias="Manager";
						$scope.userDetails.managerAlias="Manager";
					}
					
				/*}*/
				
				GlobalModule_dataStoreService.storeData('UserModule','userId',userId);
				//GlobalModule_dataStoreService.storeData('UserModule','userDetails',userDetails);
				GlobalModule_dataStoreService.storeData('UserModule','userDetails',$scope.userDetails);
				GlobalModule_dataStoreService.storeData('UserModule','loggedIn',loggedIn);
				GlobalModule_dataStoreService.storeData('UserModule','menuList',menuList);
				$location.path('/pinenter');
				
			}
	     else if(loggedIn=="true"){
				$scope.userDetails = JSON.parse(userDetails);
			/*	if($scope.userDetails.roleObj.id!=3 ){*/
					if(!$scope.userDetails.organizationAlias){
						$rootScope.userDetails.organizationAlias="Organization";
						$scope.userDetails.organizationAlias="Organization";
					}
					if(!$scope.userDetails.batchAlias){
						$rootScope.userDetails.batchAlias="Batch";
						$scope.userDetails.batchAlias="Batch";
					}
					if(!$scope.userDetails.managerAlias){
						$rootScope.userDetails.managerAlias="Manager";
						$scope.userDetails.managerAlias="Manager";
					}
					
				//}
				
				GlobalModule_dataStoreService.storeData('UserModule','userId',userId);
				//GlobalModule_dataStoreService.storeData('UserModule','userDetails',userDetails);
				GlobalModule_dataStoreService.storeData('UserModule','userDetails',$scope.userDetails);
				GlobalModule_dataStoreService.storeData('UserModule','loggedIn',loggedIn);
				GlobalModule_dataStoreService.storeData('UserModule','menuList',menuList);
				
				location.href = '#/dashboard';
				
		}else{
			
			GlobalModule_dataStoreService.storeData('UserModule','userDetails',undefined);
			GlobalModule_dataStoreService.storeData('UserModule','loggedIn',undefined);
			GlobalModule_dataStoreService.storeData('UserModule','userId',undefined);
			//location.href = '#/login';
			
		}
	//}
	};
	$scope.checkLogin(); 
	
	$scope.forgotPassword = function(user) {
		$(".loader").show();
		console.log(user);
		UserModule_loginService.loginUser(user).then(function(response){
			//Success
			GlobalModule_notificationService.notification("success","UserModule","Password is send on email Please check.");
			$(".loader").fadeOut('slow');
	  		location.href = '#/login';
	
		},function(response){
			//Error		
			$(".loader").fadeOut('slow');
			GlobalModule_notificationService.notification("success","UserModule","Please Enter correct email Id");
		});
	};
	
	$scope.logout = function(){
		
		$(".loader").show();
		$rootScope.userDetails=undefined;
		GlobalModule_dataStoreService.storeData('UserModule','userDetails',undefined);
		 GlobalModule_dataStoreService.storeData('AttendenceModule','managerName',$scope.managerDetails);
		 GlobalModule_dataStoreService.storeData('AttendenceModule','orgId',null);
		 GlobalModule_dataStoreService.storeData('AttendenceModule','batchId',null);
		 GlobalModule_dataStoreService.storeData('AttendenceModule','batchName',null);
		 GlobalModule_dataStoreService.storeData('AttendenceModule','dateAttn',null);
		//$("#alertmodal").modal();
		 
		    localStorage.removeItem("userDetails");
			localStorage.removeItem("userid");
			localStorage.removeItem("userpin");
			localStorage.removeItem("isLoggedIn");
			//localStorage.removeItem("userpin");
			
		 $(".loader").fadeOut('slow');
		$location.path('/login');
		 $(".loader").fadeOut('slow');
	};
	
	
	
	  
	  $scope.isEmailnameFound= function(userMailId){
		  console.log(userMailId);
		  $(".loader").show();
		//	var abc=$("#email").val();
			//alert(abc);
			
			GlobalModule_dataStoreService.storeData('UserModule','forgetEmail',userMailId);
			UserModule_loginService.isEmailnameFound(userMailId).then(function(response){
			
				$scope.isemailfound=response.data;
				  console.log($scope.isemailfound);
					if($scope.isemailfound){
						document.getElementById("email").innerHTML = "";
						$scope.user.email=undefined;
						GlobalModule_notificationService.notification("success","UserModule","Email Id not Present");
					}else{
							 $(".loader").fadeOut('slow');
							 $scope.userInfo = response.data;
							 console.log($scope.userInfo );
							 //GlobalModule_notificationService.notification("success","UserModule","Please login with your new Password");
							 $location.path('/recoverpassword');
							 //alert($scope.userInfo );
							 //GlobalModule_notificationService.notification("Please check your email to continue. Check your spam folder if you do not receive a mail in your inbox.");
							 GlobalModule_notificationService.notification("success","UserModule","Please check your email to continue. Check your spam folder if you do not receive a mail in your inbox.");
					}
					
					$(".loader").fadeOut('slow');
			},function(response){
				$(".loader").fadeOut('slow');
				//Error
				GlobalModule_notificationService.notification("success","UserModule","Invalid Email Id");
			});


		};
		
		
		$scope.resetPassword = function(email){
			 $(".loader").show();
			//$scope.email=GlobalModule_dataStoreService.loadData('UserModule','forgetEmail');
			$scope.user={};
			$scope.user.email = email;
			//alert($scope.user.email);
			 UserModule_loginService.resetPassword($scope.user).then(function(response){
				 $(".loader").fadeOut('slow');
				$scope.userInfo = response.data;
				console.log($scope.userInfo );
				//GlobalModule_notificationService.notification("success","UserModule","Please login with your new Password");
				$location.path('/recoverpassword');
				//alert($scope.userInfo );
				//GlobalModule_notificationService.notification("Please check your email to continue. Check your spam folder if you do not receive a mail in your inbox.");
				GlobalModule_notificationService.notification("success","UserModule","Please check your email to continue. Check your spam folder if you do not receive a mail in your inbox.");
				},function(response){
					$(".loader").fadeOut('slow');
					//Error	
					GlobalModule_notificationService.notification("success","UserModule","Invalid Email Id");

				});
			};
			
			$scope.checkotp=function(otp){
				$(".loader").show();
				var abc=$("#otp1").val();
			
				$scope.user={};
				$scope.user.otp1=abc;
				
				$scope.opt=otp;
				UserModule_loginService.checkOTP($scope.opt).then(function(response){
					$(".loader").fadeOut('slow');
					$scope.checkotpresponse=response.data;
					console.log($scope.checkotpresponse);
					if($scope.checkotpresponse==false){
						//GlobalModule_notificationService.notification("Wrong Otp, 'error'");
						GlobalModule_notificationService.notification("success","UserModule","Invalid OTP");
					//	alert("Wrong otp");
						
						$("#otp1").val("");
						}else{
							$(".loader").fadeOut('slow');
							$location.path('/confirmpassword');
						
						}
					//$scope.authorisedFlag=response.data.authorisedFlag;
					//$(".loader").fadeOut("slow");
				},function(response){
					$(".loader").fadeOut('slow');
					GlobalModule_notificationService.notification("success","Invalid OTP");
					//$(".loader").fadeOut("slow");
				});
			};
			
			
			$scope.createPassword=function(){
				$(".loader").show();
				$scope.newpassword=$("#password").val(); 
				$scope.passwordConfirmed=$("#password_c").val();
				if($scope.newpassword != $scope.passwordConfirmed){
					//alert($scope.passwordConfirmed);
					$(".loader").fadeOut('slow');
					GlobalModule_notificationService.notification("success","UserModule","Password does not match");
					return;
				}
			else{
				$scope.email=GlobalModule_dataStoreService.loadData('UserModule','forgetEmail');
				$scope.user = {};
				$scope.user.password = $scope.newpassword;
				$scope.user.email = $scope.email;
				UserModule_loginService.createPassword($scope.user).then(function(response){
					$(".loader").fadeOut('slow');
					$scope.createPasswordresponse=response.data;
					GlobalModule_dataStoreService.storeData('UserModule','forgetEmail',undefined);
					GlobalModule_notificationService.notification("success","UserModule","Password has Changed");
					$location.path('/login');
					//$(".loader").fadeOut("slow");
				},function(response){
					$(".loader").fadeOut('slow');
					//$(".loader").fadeOut("slow");
					GlobalModule_notificationService.notification("success","UserModule","password not matched");
				});
			}
			};
			
			
			
			
			$scope.generateOTP=function(){
				$(".loader").show();
				$scope.email=GlobalModule_dataStoreService.loadData('UserModule','forgetEmail');
				$scope.user={};
				$scope.user.email = $scope.email;
				 UserModule_loginService.resetPassword($scope.user).then(function(response){
					 $(".loader").fadeOut('slow');
						$scope.userInfo = response.data;
						GlobalModule_notificationService.notification("success","UserModule","Please check your email to continue. Check your spam folder if you do not receive a mail in your inbox.");
						
					//$(".loader").fadeOut("slow");
					//GlobalModule_notificationService.notification("One Time Password(OTP) has been sent to your mail, please enter it here to verify your email.");
				},function(response){
					$(".loader").fadeOut('slow');
					//$(".loader").fadeOut("slow");
				});
			};
			
			$scope.getAppVersion=function(){
				$(".loader").show();
				 UserModule_loginService.getAppVersion().then(function(response){
					 $(".loader").fadeOut('slow');
						$scope.appversiondata = response.data;
						var mylocalAppVersion = localStorage.getItem("appVersion");
					
						if(mylocalAppVersion < $scope.appversiondata.appversion){
							
							location.href = '#/updateAppVerion';
						}
				 },function(response){
					$(".loader").fadeOut('slow');
					
				});
			};
			$scope.getAppVersion();

			
			/* $("#btnSubmit").click(function () {
		            var password = $("#password").val();
		            var confirmPassword = $("#password_c").val();
		            if (password != confirmPassword) {
		            	GlobalModule_notificationService.notification("success","UserModule","Password does not match");
		                return false;
		            }
		            return true;
		        });*/
			
			
			/*$scope.isPasswordMatch = function(newpassword,passwordConfirmed) {
				if(newpassword==passwordConfirmed){
					$scope.passwordMatch= true;
				}else{
					$scope.passwordMatch = false;
					GlobalModule_notificationService.notification("success","UserModule","Password does not match");
				}
			};*/
			
			/* $scope.fetchMemberDetails = function(memberId){
	    			MemberModule_Service.fetchMemberDetails(memberId).then(function(response){
	    				//Success
	    				$(".loader").fadeOut('slow');
	    				$scope.member = response.data;
	    				$scope.member.phoneNo= parseInt ($scope.member.phoneNo,10);
	    				 $scope.member.selectedRole =$scope.member.roleObj.id;
	    			
	    				 console.log($scope.member);
	    				GlobalModule_dataStoreService.storeData('AttendenceModule','selectedMember',$scope.member);
	    				location.href = '#/updateMember';
	    				console.log($scope.member);
	    			},function(response){
	    				//Error			
	    				$(".loader").fadeOut('slow');
	    			});
	    		};	
			
	    		 $scope.fetchMemberDetails($rootScope.userDetails.id);	*/
			
			$scope.myProfile = function() {
				console.log("myProfile");
				$scope.member =$rootScope.userDetails;
				$scope.member.phoneNo= parseInt ($scope.member.phoneNo,10);
				 $scope.member.selectedRole =$scope.member.roleObj.id;
			
				 console.log($scope.member);
				GlobalModule_dataStoreService.storeData('AttendenceModule','selectedMember',$scope.member);
				location.href = '#/updateMember';
			};
			
			
			
			
		
}]);



controllers.controller('UserModule_PinCtrl' , ['$compile','$timeout','$scope','$rootScope','$location','$route','$window','GlobalModule_dataStoreService','GlobalModule_notificationService', function ($compile,$timeout,$scope, $rootScope,$location,$route,$window,GlobalModule_dataStoreService,GlobalModule_notificationService) {


	$scope.createPin = function(user){
		//alert(JSON.stringify(user.pin));
		window.localStorage.setItem("userpin", JSON.stringify(user.pin));
				$location.path('/dashboard');
		};
		
		
		$scope.checkPin = function(userpin){
			
			//alert(JSON.stringify(userpin));
			var storedPin = localStorage.getItem("userpin");
			
			if((JSON.stringify(userpin)) == storedPin)
			{	
				$location.path('/dashboard');
			}
			else{
				
				GlobalModule_notificationService.notification("success","UserModule", "Incorrect Pin Entered!");
			}
			
		};
		
		

}]);






