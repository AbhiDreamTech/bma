'use strict';

var controllers = angular.module('UserModule');


controllers.controller('UserCtrl' , ['$scope','$location','$http','$rootScope','UserModule_loginService','GlobalModule_dataStoreService','GlobalModule_notificationService', function ($scope, $location, $http,$rootScope, UserModule_loginService,GlobalModule_dataStoreService,GlobalModule_notificationService) {
	$rootScope.userDetails=GlobalModule_dataStoreService.loadData('UserModule','userDetails');
	$scope.email=GlobalModule_dataStoreService.loadData('UserModule','email');
	
	$scope.user={};
	$scope.user.gender='Male';
	$scope.isaaNumber=false;
	
	$scope.fetchServices = function() {
		$(".loader").show();
		UserModule_loginService.fetchServices().then(function(response){
			//Success
			$(".loader").fadeOut('slow');
			$scope.services=response.data;
			console.log($scope.services);
		},function(response){
			//Error			
			$(".loader").fadeOut('slow');
		});
	};
	$scope.fetchServices();
	
	$scope.registerUser = function(user) {
	
		if(user.password != user.passwordConfirmed){
			/*GlobalModule_notificationService.notification("info","UserModule","User password should match with confirmed password.");*/
			return;
		}
		$scope.user=user;
		$scope.user.createdBy = 1;
		$scope.user.updatedBy= 1;
		$scope.user.service={};
		$(".loader").show();
	    angular.forEach($scope.services, function(service){
		      if(service.selected){
		    	  console.log("service\n"+service);
		    	  $scope.user.service.serviceRtateBenefiteId = service.serviceRtateBenefiteId;
		    	  $scope.user.service.serviceRtateBenefite ={};
		    	  $scope.user.service.serviceRtateBenefite.noOfMonth = service.serviceRtateBenefite.noOfMonth;
		      }
		    });
	   
		console.log("user\n"+JSON.stringify($scope.user.DOBString));
		
		UserModule_loginService.registerUser($scope.user).then(function(response){
			//Success
			$scope.userData=response.data;
			
			$rootScope.userDetails=$scope.userData;
	  		GlobalModule_dataStoreService.storeData('UserModule','userDetails',$rootScope.userDetails);
	  		GlobalModule_notificationService.notification("success","UserModule","User registered sucessfully.");
			
	  		$(".loader").fadeOut('slow');
	  		location.href = '#/alias';
			
		},function(response){
			//Error			
			$(".loader").fadeOut('slow');
			 GlobalModule_notificationService.notification("error","UserModule","Error while user registration .");
		});
	};

	$scope.isPasswordMatch = function(password,passwordConfirmed) {
		if(password==passwordConfirmed){
			$scope.passwordMatch= true;
		}else{
			$scope.passwordMatch = false;
		}
	};
	
$scope.isUsernameUnique= function(){
	var abc=$("#username1").val();
	UserModule_loginService.isUsernameUnique(abc).then(function(response){
			$scope.isUsernameUniqueresponse = response.data;
			console.log($scope.isUsernameUniqueresponse);
			if($scope.isUsernameUniqueresponse==false){
				document.getElementById("username1").innerHTML = "";
				$scope.user.userName=undefined;
				GlobalModule_notificationService.notification("success","UserModule","Username already exists");
			}else{

			}
	},function(response){
		//Error
	});


};


$scope.isRegisternameUnique= function(){
	var abc=$("#username1").val();
	//alert(abc);
	UserModule_loginService.isRegisternameUnique(abc).then(function(response){
			$scope.isRegisternameUniqueResponse = response.data;
			console.log($scope.isRegisternameUniqueResponse);
			if($scope.isRegisternameUniqueResponse==false){
				document.getElementById("username1").innerHTML = "";
				$scope.user.userName=undefined;
				GlobalModule_notificationService.notification("success","UserModule","Please try with different UserName");
			}else{

			}
	},function(response){
		//Error
	});


};




$scope.isaNumber = function(){
    $scope.name = new Date().getTime();
    var abc=document.getElementById("inputPhone");
    	$scope.isaaNumber= angular.isNumber(abc);
    	alert($scope.isaaNumber);
  };


  $scope.isEmailIdUnique= function(){
		var abc=$("#email1").val();
		UserModule_loginService.isEmailIdUnique(abc).then(function(response){
				$scope.isEmailIdUniqueresponse = response.data;
				console.log($scope.isEmailIdUniqueresponse);
				if($scope.isEmailIdUniqueresponse==false){
					document.getElementById("email1").innerHTML = "";
					$scope.user.email=undefined;
					GlobalModule_notificationService.notification("success","UserModule","Email Id already exists");
				}else{

				}
		},function(response){
			//Error
		});


	};
	
	
	 $scope.checkPhone = function(no){
		 var number = $("#inputPhone").val();
		if (number.length > 0 ) 
		    {
			 if (number.length >10 || number.length <10)
			   {
				 GlobalModule_notificationService.notification("success","UserModule","Phone number should be 10 digit");
				 document.getElementById("inputPhone").value = "";
			        $("#inputPhone").focus();
			        return false;
			   }
		    }
	};
	
}]);
