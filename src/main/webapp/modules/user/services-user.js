'use strict';

var services = angular.module('UserModule');

services.service('UserModule_loginService',['$rootScope','$location','$http','$route', function($rootScope, $location, $http,$route) {
	
	this.loginUser = function(users){
		var promise = $http({
			method : 'POST',
			data : users,
			url : attendanceURL+'api/user/loginUser',
			headers : {
				'Content-Type' : 'application/json'
			},
			cache : false
		}).then(function (response) {
	        return response;
	    });
		return promise;		
	};
	
	this.fetchMenu = function(roleId){
		
		var promise = $http({
			method : 'GET',
			url : attendanceURL+'api/user/fetchmenu/'+roleId,
			headers : {
				'Content-Type' : 'application/json'
			},
			cache : false
		}).then(function (response) {
	        return response;
	    });
		return promise;		
	};
	
	this.registerUser = function(user){
		var promise = $http({
			method : 'POST',
			data : user,
			url : attendanceURL+'api/user/registeruser',
			headers : {
				'Content-Type' : 'application/json'
			},
			cache : false
		}).then(function (response) {
			return response;
		});
		return promise;		
	};

	 this.fetchServices = function(){
 		var promise = $http({
 			method : 'GET',
 			url : attendanceURL+'api/dropdown/services',	
 			headers : {
 				'Accept' : 'application/json'
 			},
 			cache : false
 		}).then(function (response) {
 	        return response;
 	    });
 		return promise;		
	 };
	
	 this.addUserAlias = function(alias){
			var promise = $http({
				method : 'POST',
				data : alias,
				url : attendanceURL+'api/user/adduseralias',
				headers : {
					'Content-Type' : 'application/json'
				},
				cache : false
			}).then(function (response) {
		        return response;
		    });
			return promise;		
		};
		
	 this.fetchUserAlias = function(){
	 		var promise = $http({
	 			method : 'GET',
	 			url : attendanceURL+'api/dropdown/appdefault',	
	 			headers : {
	 				'Accept' : 'application/json'
	 			},
	 			cache : false
	 		}).then(function (response) {
	 	        return response;
	 	    });
	 		return promise;		
		 };
		 
		 
		 
		 this.isUsernameUnique = function(userName){
		 		var promise = $http({
		 			method : 'GET',
		 			url : attendanceURL+'api/user/isUsernameUnique/'+userName,	
		 			headers : {
		 				'Accept' : 'application/json'
		 			},
		 			cache : false
		 		}).then(function (response) {
		 	        return response;
		 	    });
		 		return promise;		
			 };
			 
			 
			 
			 this.isRegisternameUnique = function(userName){
				 //alert("Hi"+userName);
			 		var promise = $http({
			 			method : 'POST',
			 			//data : userName,
			 			url : attendanceURL+'api/user/isRegisternameUnique/'+userName,	
			 			headers : {
			 				'Accept' : 'application/json'
			 			},
			 			cache : false
			 		}).then(function (response) {
			 	        return response;
			 	    });
			 		return promise;		
				 };
			 
			 
			 
			 this.showalias = function(id){
					var promise = $http({
						method : 'GET',
						//data : users,
						url : attendanceURL+'api/user/showalias/'+id,
						headers : {
							'Content-Type' : 'application/json'
						},
						cache : false
					}).then(function (response) {
				        return response;
				    });
					return promise;		
				};
				
				
				
				 this.isEmailnameFound = function(email){
					    //alert("Hi"+email);
				 		var promise = $http({
				 			method : 'POST',
				 			data : email,
				 			url : attendanceURL+'api/user/isEmailnameFound/'+email,	
				 			headers : {
				 				'Accept' : 'application/json'
				 			},
				 			cache : false
				 		}).then(function (response) {
				 	        return response;
				 	    });
				 		return promise;		
					 };
				
				this.resetPassword= function(user){
					var promise = $http({
						method : 'POST',
						data :user,
						url : attendanceURL+'api/user/resetpassword',
						headers : {
							'Content-Type' : 'application/json'
						},
						cache : false
					}).then(function (response) {
				        return response;
				    });
					return promise;		
					
				};
				
				
				
				this.generateOTP = function(user){
					//alert(user);
					var promise = $http({
						method : 'POST',
						data : user,
						url : attendanceURL+ 'api/user/generateotp',
						headers : {
							'Content-Type' : 'application/json'
						},
						cache : false
					}).then(function (response) {
						return response;
					});
					return promise;		
				};
				
				this.checkOTP = function(otp){
					var promise = $http({
						method : 'POST',
						data : otp,
						url : attendanceURL+ 'api/user/checkotp/'+otp,
						headers : {
							'Content-Type' : 'application/json'
						},
						cache : false
					}).then(function (response) {
						return response;
					});
					return promise;		
				};

				
				this.createPassword = function(password){
					var promise = $http({
						method : 'POST',
						data : password,
						url :  attendanceURL+ 'api/user/createPassword',
						headers : {
							'Content-Type' : 'application/json'
						},
						cache : false
					}).then(function (response) {
						return response;
					});
					return promise;		
				};
				
				
				this.isEmailIdUnique = function(email){
					 //alert("Hi"+userName);
				 		var promise = $http({
				 			method : 'POST',
				 			data : email,
				 			url : attendanceURL+'api/user/isEmailIdUnique/'+email,	
				 			headers : {
				 				'Accept' : 'application/json'
				 			},
				 			cache : false
				 		}).then(function (response) {
				 	        return response;
				 	    });
				 		return promise;		
					 };
					 
						this.getAppVersion = function(){
							
							var promise = $http({
								method : 'GET',
								url : attendanceURL+'api/user/getupdatedappversion',
								headers : {
									'Content-Type' : 'application/json'
								},
								cache : false
							}).then(function (response) {
						        return response;
						    });
							return promise;		
						};
}]);
