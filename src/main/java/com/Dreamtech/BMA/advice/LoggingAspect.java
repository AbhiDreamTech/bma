package com.Dreamtech.BMA.advice;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.Dreamtech.BMA.util.Constants;

@Aspect
public class LoggingAspect {
	@Before("execution(* com.Dreamtech.BMA.dao.impl.*.*(..))")
	public void logBefore(JoinPoint joinPoint) {

		System.out.println("In method : " + joinPoint.getSignature().getName());
	}
	@After("execution(* com.Dreamtech.BMA.dao.impl.*.*(..))")
	public void logAfter(JoinPoint joinPoint) {
		System.out.println("Out of method : " + joinPoint.getSignature().getName());
	}

	@AfterThrowing(
			pointcut = "execution(* com.Dreamtech.BMA.dao.impl.*.*(..))",
			throwing= "error")
	public void logAfterThrowing(JoinPoint joinPoint, Throwable error) {
		System.out.println("logAfterThrowing() is running!");
		System.out.println("Exception : " + error);
	}
}
