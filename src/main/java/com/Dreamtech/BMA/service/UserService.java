package com.Dreamtech.BMA.service;

import java.text.ParseException;
import java.util.List;

import com.Dreamtech.BMA.bean.Role;
import com.Dreamtech.BMA.bean.User;


public interface UserService {

	User registerUser(User user) throws ApplicationException;

    User loginUser(User user) throws ApplicationException;
   	
}