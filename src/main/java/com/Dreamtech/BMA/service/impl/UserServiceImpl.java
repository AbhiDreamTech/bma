package com.Dreamtech.BMA.service.impl;

import java.text.ParseException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Dreamtech.BMA.bean.Role;
import com.Dreamtech.BMA.bean.User;
import com.Dreamtech.BMA.dao.DataAccessLayerException;
import com.Dreamtech.BMA.dao.UserDao;
import com.Dreamtech.BMA.service.ApplicationException;
import com.Dreamtech.BMA.service.UserService;
import com.Dreamtech.BMA.util.Encryptor;
import com.Dreamtech.BMA.util.Utils;


@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {
	
	@Autowired private UserDao userDao;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Override
	public User registerUser(User user) throws ApplicationException {
		try {
			user.setPassword(Encryptor.encrypt(Encryptor.key, user.getPassword()));
			User userData= userDao.registerUser(user);
			return userData;
			
		} catch (DataAccessLayerException exDataAccessLayer) {
			exDataAccessLayer.getCause().printStackTrace();
			throw new ApplicationException("Application error while updating user details", exDataAccessLayer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ApplicationException("Application error while encrypting", e);
		}
	}

	@Override
	public User loginUser(User user) throws ApplicationException {
		try {
				
				User userData = userDao.loginUser(user);
				return userData;
				
		}catch (EmptyResultDataAccessException excpDataAccessLayer) {	
					LOGGER.error("Incorrect username or password.", excpDataAccessLayer);
					throw new EmptyResultDataAccessException("Incorrect username or password.", 1);
		} catch (DataAccessLayerException excpDataAccessLayer) {
				LOGGER.error("Application error while login : " + excpDataAccessLayer.getMessage());
				throw new ApplicationException("Application error while login.", excpDataAccessLayer);
		}
	
		}
	
	
}
