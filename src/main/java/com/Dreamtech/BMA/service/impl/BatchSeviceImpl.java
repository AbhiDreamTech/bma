package com.Dreamtech.BMA.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.Dreamtech.BMA.bean.Batch;
import com.Dreamtech.BMA.bean.User;
import com.Dreamtech.BMA.dao.BatchDao;
import com.Dreamtech.BMA.dao.DataAccessLayerException;
import com.Dreamtech.BMA.service.ApplicationException;
import com.Dreamtech.BMA.service.BatchService;

@Service
@Transactional(readOnly = true)
public class BatchSeviceImpl implements BatchService{

	@Autowired private BatchDao batchDao;

	private static final Logger LOGGER = LoggerFactory.getLogger(BatchSeviceImpl.class);
	
	@Override
	public void addBatch(Batch batch) throws ApplicationException {
		// TODO Auto-generated method stub
	
		try {
			batchDao.addBatch(batch);
		} catch (DataAccessLayerException exDataAccessLayer) {
			exDataAccessLayer.getCause().printStackTrace();
			throw new ApplicationException("Application error while updating user details", exDataAccessLayer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getCause().printStackTrace();
			throw new ApplicationException("Application error while encrypting", e);
		}
		
	}

	@Override
	public List<Batch> fetchBatchList(int orgid) throws ApplicationException {
		try{
			return batchDao.fetchBatchList(orgid);
		}catch(DataAccessLayerException exp){
			exp.printStackTrace();
			throw new ApplicationException(exp.getMessage(), exp);
		}
	}

	@Override
	public List<Batch> fetchBatchDeatilList(int id) throws ApplicationException {
		// TODO Auto-generated method stub
		try{
			return batchDao.fetchBatchDeatilList(id);
		}catch(DataAccessLayerException exp){
			exp.printStackTrace();
			throw new ApplicationException(exp.getMessage(), exp);
		}
	}

	@Override
	public Boolean isBatchNameUnique(String batchName, int userId)
			throws ApplicationException {
		try {
			Boolean userObj= batchDao.isBatchNameUnique(batchName, userId);
			return userObj;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ApplicationException("Application error validating Username", e);
		}
	}

	@Override
	public void updateBatch(Batch batch) throws ApplicationException {
		try {
			batchDao.updateBatch(batch);
		} catch (DataAccessLayerException exDataAccessLayer) {
			exDataAccessLayer.getCause().printStackTrace();
			throw new ApplicationException("Application error while updating user details", exDataAccessLayer);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.getCause().printStackTrace();
			throw new ApplicationException("Application error while encrypting", e);
		}
	}

	@Override
	public List<Batch> fetchBatchListByManagerId(int id)
			throws ApplicationException {
		try{
			return batchDao.fetchBatchListByManagerId(id);
		}catch(DataAccessLayerException exp){
			exp.printStackTrace();
			throw new ApplicationException(exp.getMessage(), exp);
		}
	}


}