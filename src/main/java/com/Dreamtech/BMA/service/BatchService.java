package com.Dreamtech.BMA.service;

import java.util.List;

import com.Dreamtech.BMA.bean.Batch;

public interface BatchService {

	void addBatch(Batch batch) throws ApplicationException;

	List<Batch> fetchBatchList(int orgId) throws ApplicationException;

	//List<Batch> fetchBatchDeatilList(int id, int roleId)throws ApplicationException;
	
	List<Batch> fetchBatchDeatilList(int id) throws ApplicationException;

	Boolean isBatchNameUnique(String batchName, int userId) throws ApplicationException;

	void updateBatch(Batch batch) throws ApplicationException;

	List<Batch> fetchBatchListByManagerId(int id)  throws ApplicationException;
}
