package com.Dreamtech.BMA.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Dreamtech.BMA.bean.Role;
import com.Dreamtech.BMA.bean.User;
import com.Dreamtech.BMA.service.ApplicationException;
import com.Dreamtech.BMA.service.UserService;
import com.Dreamtech.BMA.util.Constants;
import com.Dreamtech.BMA.util.ErrorMessageBuilder;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

@Component
@Path("/user")

public class UserResource extends AbstractResource {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserResource.class);

	@Autowired private UserService userService;

	@Path("/registeruser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response registerUser(User user){
		//LOGGER.debug(Constants.LOG_METHOD_ENTRY);		
		Response response = null;
		try{				
			User userData = userService.registerUser(user);
			response = Response.ok(userData.toString()).build();
		}catch(ApplicationException exApplication){
			LOGGER.error(exApplication.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(exApplication)).build();
		}

		//LOGGER.debug(Constants.LOG_METHOD_EXIT);
		return response;
	
	}
	
	
	
	@Path("/loginUser")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)

	public Response loginUser(User user){
		Response response= null;
		try{				
			User userData = userService.loginUser(user);
			response = Response.ok(userData.toString()).build();
		}catch(ApplicationException exApplication){
			LOGGER.error(exApplication.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(exApplication)).build();
		}
		//LOGGER.debug(Constants.LOG_METHOD_EXIT);
		
		return response;
	}
	}
	