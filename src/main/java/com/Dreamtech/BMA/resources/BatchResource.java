package com.Dreamtech.BMA.resources;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.Dreamtech.BMA.bean.Batch;
import com.Dreamtech.BMA.bean.User;
import com.Dreamtech.BMA.service.ApplicationException;
import com.Dreamtech.BMA.service.BatchService;
import com.Dreamtech.BMA.util.ErrorMessageBuilder;

@Component
@Path("/batch")
public class BatchResource extends AbstractResource{

	private static final Logger LOGGER = LoggerFactory.getLogger(BatchResource.class);
	@Autowired private BatchService batchService;
	
	@Path("/addbatch")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response addBatch(Batch batch){
		//LOGGER.debug(Constants.LOG_METHOD_ENTRY);		
		Response response = null;
		try{				
			batchService.addBatch(batch);
			response = Response.status(Status.OK).build();
		}catch(ApplicationException exApplication){
			LOGGER.error(exApplication.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(exApplication)).build();
		}

		//LOGGER.debug(Constants.LOG_METHOD_EXIT);
		return response;
	}
	
	@Path("/fetchbatch/{orgid}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchOrganazationlist(@PathParam("orgid") final int orgid){
		Response response = null;
		try {
			List<Batch> services = batchService.fetchBatchList(orgid);
			response = Response.ok(convertToJson(services)).build();
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(e)).build();
		}
		return response;
	}
	
	
	
	@Path("/fetchBatchDeatilList/{id}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchBatchDeatilList(@PathParam("id") final int id){
		Response response = null;
		try {
			List<Batch> services = batchService.fetchBatchDeatilList(id);
			response = Response.ok(convertToJson(services)).build();
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(e)).build();
		}
		return response;
	}
	
	
	@Path("/isBatchNameUnique/{batchName}/{userId}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response isUsernameUnique(@PathParam("batchName") final String batchName, @PathParam("userId") final int userId){
		Response response = null;
		try {
			Boolean user = batchService.isBatchNameUnique(batchName, userId);
			response = Response.ok(convertToJson(user)).build();
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(e)).build();
		}
		return response;
	}
	
	@Path("/updatebatch")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateBatch(Batch batch){
		//LOGGER.debug(Constants.LOG_METHOD_ENTRY);		
		Response response = null;
		try{				
			batchService.updateBatch(batch);
			response = Response.status(Status.OK).build();
		}catch(ApplicationException exApplication){
			LOGGER.error(exApplication.getMessage());
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(exApplication)).build();
		}

		//LOGGER.debug(Constants.LOG_METHOD_EXIT);
		return response;
	}
	
	@Path("/fetchBatchListByManagerId/{id}")
	@GET
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response fetchBatchListByManagerId(@PathParam("id") final int id){
		Response response = null;
		try {
			List<Batch> services = batchService.fetchBatchListByManagerId(id);
			response = Response.ok(convertToJson(services)).build();
		} catch (ApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response = Response.status(Status.INTERNAL_SERVER_ERROR).entity(ErrorMessageBuilder.buildJsonMessage(e)).build();
		}
		return response;
	}
	
}
