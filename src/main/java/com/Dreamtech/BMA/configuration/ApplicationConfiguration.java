package com.Dreamtech.BMA.configuration;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.PlatformTransactionManager;

import com.Dreamtech.BMA.util.Log4jLevelChanger;

@Configuration
@ComponentScan(basePackages="com.planfirma.sscpl")
@PropertySource("classpath:properties/database.properties")
@ImportResource("classpath:spring/spring-config.xml")
public class ApplicationConfiguration {
	private static final String PROPERTY_NAME_DATABASE_DRIVER = "jdbc.driverClassName";
    private static final String PROPERTY_NAME_DATABASE_PASSWORD = "jdbc.password";
    private static final String PROPERTY_NAME_DATABASE_URL = "jdbc.url";
    private static final String PROPERTY_NAME_DATABASE_USERNAME = "jdbc.username";
    
	@Resource
    private Environment env;
	 @Bean
     public DataSource actualDataSource() {
             DriverManagerDataSource dataSource = new DriverManagerDataSource();
             dataSource.setDriverClassName(env.getRequiredProperty(PROPERTY_NAME_DATABASE_DRIVER));
             dataSource.setUrl(env.getRequiredProperty(PROPERTY_NAME_DATABASE_URL));
             dataSource.setUsername(env.getRequiredProperty(PROPERTY_NAME_DATABASE_USERNAME));
             dataSource.setPassword(env.getRequiredProperty(PROPERTY_NAME_DATABASE_PASSWORD));
              
             return dataSource;
     }
	 @Bean
	 public PlatformTransactionManager transactionManager() {
         return new DataSourceTransactionManager(actualDataSource());
     }
	/* @Bean
	 public AbstractDao abstractDao(){
		 AbstractDao abstractDao=new AbstractDao();
		 abstractDao.setDataSource(actualDataSource());
		 abstractDao.setTransactionManager(transactionManager());
		return abstractDao;
	 }*/
	 
	@Bean
	public Log4jLevelChanger log4jLevelChanger(){
		return new Log4jLevelChanger();
	}
	/*@Bean
	public DropdownDaoImpl dropdownDaoImpl(){
		DropdownDaoImpl dropdownDaoImpl=new DropdownDaoImpl();
		initAbstractDao(dropdownDaoImpl);
		return dropdownDaoImpl;
	}
	private void initAbstractDao(AbstractDao abstractDao) {
		abstractDao.setDataSource(actualDataSource());
		abstractDao.setTransactionManager(transactionManager());
		
	}*/
      
}
