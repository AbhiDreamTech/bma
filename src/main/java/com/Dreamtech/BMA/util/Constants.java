package com.Dreamtech.BMA.util;

import java.util.Properties;


public final class Constants {

	public final static String LOG_METHOD_ENTRY = ">>>";
	public final static String LOG_METHOD_EXIT = "<<<";
	public final static String APP_DATE_FORMAT = "MM/dd/yyyy";
	
	public final static String GRAB = "Grab";
	public final static String APPLICATION = "Application";
	public final static String DEPN_METHODS[] = {"SLM","WDV"};
	
	
	private Constants() {
	}
	
	public interface applicationLanguageConstats{
		public final static String EN_US = "en-us";
		public final static String EN = "en";
	}
	
	public interface QueryOperationType{
		public final String DELETE = "DELETE";
		public final String INSERT = "INSERT";
		public final String UPDATE = "UPDATE";
	}
	
	
	public interface VTiger{
		public final String BASE_URL = "http://localhost:8888/";		
		public final static String LEADS_MODULE = "Leads";
		public final static String TICKETS_MODULE = "HelpDesk";
	}
	
	public interface Solr{
		public final String Server_URL = "http://localhost:8080/solr";		
	}
	
	public interface LoginType{
		public final String SYSTEM = "SYSTEM";		
		public final static String FACEBOOK = "FACEBOOK";
		public final static String GOOGLEPLUS = "GOOGLEPLUS";
	}
	
	public interface FileUploadFolders{
	//	public final static String TOMCAT_HEALTHASSURE="D:\\Soft SETUP\\software setups 26 july 13\\apache-tomcat-7.0.42-windows-x64\\apache-tomcat-7.0.42\\webapps\\healthAssureDocs";
		public final static String SERVER_PATH="/var//lib//tomcat8//webapps//";
		public final static String TOMCAT_HEALTHASSURE="SSCPLImage//profileImage//";
		public static final String APPLICATION_PATH = "//webapps//";
		
		public final static String TESTDOC="//TESTDOC//";

		public static final String ORG_LOGO = null;
		
	}
	
}