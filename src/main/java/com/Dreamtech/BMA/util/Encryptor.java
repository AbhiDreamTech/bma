package com.Dreamtech.BMA.util;

import java.io.UnsupportedEncodingException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Hex;

public class Encryptor {
	    public final static String key="sscplplanfirma";
	    
	    public static SecretKeySpec generateMySQLAESKey(final String key, final String encoding) {
			try {
				final byte[] finalKey = new byte[16];
				int i = 0;
				for(byte b : key.getBytes(encoding))
					finalKey[i++%16] ^= b;			
				return new SecretKeySpec(finalKey, "AES");
			} catch(UnsupportedEncodingException e) {
				throw new RuntimeException(e);
			}
		}
	 
	    public static String encrypt(String key,String valueToBeEncrypted) throws Exception{
	    	final Cipher encryptCipher = Cipher.getInstance("AES");	        				
			encryptCipher.init(Cipher.ENCRYPT_MODE, generateMySQLAESKey(key, "UTF-8"));		
			System.out.println(String.format("Select aes_decrypt(unhex('%s'), '"+key+"');",
					new String(Hex.encodeHex(encryptCipher.doFinal(valueToBeEncrypted.getBytes("UTF-8")))))); 
			return new String(Hex.encodeHex(encryptCipher.doFinal(valueToBeEncrypted.getBytes("UTF-8"))));
	    }
	    public static String decrypt(String key,String valueToBeDecrypted) throws Exception{
	    	final Cipher decryptCipher = Cipher.getInstance("AES");	             				
	    	decryptCipher.init(Cipher.DECRYPT_MODE, generateMySQLAESKey(key, "UTF-8"));	
	    	System.out.println(new String(decryptCipher.doFinal(Hex.decodeHex(valueToBeDecrypted.toCharArray()))));
	   	 
			return new String(decryptCipher.doFinal(Hex.decodeHex(valueToBeDecrypted.toCharArray())));
	    }
		public static void main(String... args) throws Exception {
			encrypt("sscplplanfirma","123");
			decrypt("sscplplanfirma","211f52809c6e482617b96c49717fd1b3");
			
			/*String startDateString = "06/27/2007";
		    DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
		    java.util.Date startDate;
		    try {
		        startDate = df.parse(startDateString);
		        String newDateString = df.format(startDate);
		        System.out.println(newDateString);
		    } catch (ParseException e) {
		        e.printStackTrace();
		    }*/
	            
			//decrypt("sscplplanfirma","hello");
		}
}
