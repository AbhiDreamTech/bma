package com.Dreamtech.BMA.util;

import org.apache.commons.lang.RandomStringUtils;

public class Utils {
	public static  String generateRandomToken(){
		return RandomStringUtils.random(5, true, true);
	}
}
