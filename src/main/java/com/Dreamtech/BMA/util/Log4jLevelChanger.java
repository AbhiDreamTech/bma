package com.Dreamtech.BMA.util;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class Log4jLevelChanger {
	public void setLogLevel(String loggerName, String level) {
		final Logger logger = Logger.getLogger(loggerName);
		
		if ("trace".equalsIgnoreCase(level)) {
			logger.setLevel(Level.TRACE);
		} else if ("debug".equalsIgnoreCase(level)) {
			logger.setLevel(Level.DEBUG);
		} else if ("info".equalsIgnoreCase(level)) {
			logger.setLevel(Level.INFO);
		} else if ("error".equalsIgnoreCase(level)) {
			logger.setLevel(Level.ERROR);
		} else if ("fatal".equalsIgnoreCase(level)) {
			logger.setLevel(Level.FATAL);
		} else if ("warn".equalsIgnoreCase(level)) {
			logger.setLevel(Level.WARN);
		}
	}
}
