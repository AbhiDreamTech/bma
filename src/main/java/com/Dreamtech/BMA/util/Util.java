package com.Dreamtech.BMA.util;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.Map.Entry;

import com.Dreamtech.BMA.bean.AbstractBean;


public class Util {

	public static String type=null;
	public static String ctype=null;

	public static String category=null;
	public static String cat=null;
	public static String error=null;
	
	static PropertiFileConnection connectconstConfig = new PropertiFileConnection();
	
	public static String  generateUniqueUUID() {
		UUID uniqueKey = UUID.randomUUID();   
		return uniqueKey.toString();
	} 
	
	public static Properties loadQueries(){
		Properties prop = new Properties();
		try {
			prop.load(AbstractBean.class.getClassLoader().getResourceAsStream("databaseQueries.properties"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return prop;
	}
	
	public static Properties loadConstants(){
		Properties prop = new Properties();
		try {
			prop.load(AbstractBean.class.getClassLoader().getResourceAsStream("constants.properties"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return prop;
	}
	public static Properties loadConfiguration(){
		Properties prop = new Properties();
		try {
			prop.load(AbstractBean.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return prop;
	}
	
	public static Properties getProperties(){
		Properties prop = new Properties();
		try {
			prop.load(AbstractBean.class.getClassLoader().getResourceAsStream("config.properties"));
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		return prop;
	}
	
	
	//for uploaded docs
	public static String saveFile(InputStream uploadedInputStream,
			String serverLocation,String filename) {
		String storepath="";
		try {
			storepath=connectconstConfig.getPropValues().getProperty("constant.storePath");
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String path=storepath+serverLocation;
		
		System.out.println("path is in util "+path);
		File targetFolder = new File(path);
		String newFileName=filename;
	
		for(int i=0;i<newFileName.length();i++)	{
	
			if(newFileName.charAt(i)==' ' || newFileName.charAt(i)=='!'|| newFileName.charAt(i)=='@' || newFileName.charAt(i)=='#' || newFileName.charAt(i)=='$' || newFileName.charAt(i)=='%' || newFileName.charAt(i)=='^' || newFileName.charAt(i)=='&' || newFileName.charAt(i)=='*' ||newFileName.charAt(i)=='(' || newFileName.charAt(i)==')' || newFileName.charAt(i)=='=' || newFileName.charAt(i)=='+'  )
			{
				newFileName=newFileName.replace(newFileName.charAt(i),'_');
				//System.out.println("new file names is "+newFileName);
			}
			
		}

		//System.out.println("final new file name"+newFileName);
		String filenameGeneratd=generateUniqueUUID()+newFileName;
		if (!targetFolder.exists()) {
			targetFolder.mkdirs();
		}
		
		try {
			OutputStream outpuStream = new FileOutputStream(new File(path+filenameGeneratd));
			int read = 0;
			byte[] bytes = new byte[1024];

			outpuStream = new FileOutputStream(new File(path+filenameGeneratd));
			while ((read = uploadedInputStream.read(bytes)) != -1) {
				outpuStream.write(bytes, 0, read);
			}
			outpuStream.flush();
			outpuStream.close();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return storepath+serverLocation+filenameGeneratd;
     
	}
	
	
	public static String hashPassword(final String password) {
		if(null != password){
			MessageDigest mDigest;
			try {
				mDigest = MessageDigest.getInstance("SHA1");
				byte[] result = mDigest.digest(password.getBytes());
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < result.length; i++) {
					sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16)
							.substring(1));
				}
				return (sb.toString());
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return null;
			}
		}else{
			return null;
		}
	}
	
	
	public static Map<String,String> converMapObjectToMapString(Map<String,Object> mapFrom){		
		Map<String,String> mapTo = new HashMap<String, String>();
		for (Entry<String,Object> entry : mapFrom.entrySet()) {
			if(null != entry.getValue()){	
				System.out.println(entry.getKey());
				if(EmailSender.EMAIL_CONTENT.equals(entry.getKey())){
					String s = new String((byte[])entry.getValue());
					mapTo.put(entry.getKey(),s);
				}else{
					mapTo.put(entry.getKey(), entry.getValue().toString());
				}
			}
		}
		return mapTo;
	} 
	
}
