package com.Dreamtech.BMA.dao;

/**
 * Generic class to indicate exceptional situations at the DAO layer.
 * DAOs will throw instances of this class, while Service layer will usually catch them.   
 */
public class DataAccessLayerException extends Exception {
	
	private static final long serialVersionUID = 18349323234328L;

	public DataAccessLayerException(String errorMessage, Throwable cause) {
		super(errorMessage, cause);
	}

}
