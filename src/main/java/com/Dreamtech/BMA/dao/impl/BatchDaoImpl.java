package com.Dreamtech.BMA.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;

import com.Dreamtech.BMA.bean.Batch;
import com.Dreamtech.BMA.dao.AbstractDao;
import com.Dreamtech.BMA.dao.BatchDao;
import com.Dreamtech.BMA.dao.DataAccessLayerException;

public class BatchDaoImpl extends AbstractDao implements BatchDao {
	static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BatchDaoImpl.class);
	@Override
	public void addBatch(final Batch batch)
			throws DataAccessLayerException {
		// TODO Auto-generated method stub
		
		final StringBuilder queryString = new StringBuilder();
	
		queryString.append("INSERT INTO batch (`batchName`,`fromTime`,`toTime`, `orgId`, `isActive`, `profilePic`,`createdby`, `createdOn`, `updatedBy`, `updatedOn`,`col1_alias1`,`col2_alias2`,`col3_alias3`,`col1_enbl1`,`col2_enbl2`, `col2_enbl3`) ");
        queryString.append(" VALUES(?,?,?,?,TRUE,?,?,NOW(),?,NOW(),?,?,?,?,?,?)");

		try {
			getJdbcTemplate().update(new PreparedStatementCreator(){
				public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{					
					PreparedStatement ps = connection.prepareStatement(queryString.toString());
					ps.setString(1,batch.getBatchName());
					ps.setString(2, batch.getFromTime());
					ps.setString(3, batch.getToTime());
					ps.setInt(4, batch.getOrgId());
				    ps.setString(5, batch.getProfilePic());
				    ps.setInt(6, batch.getCreatedBy());
				    ps.setInt(7, batch.getUpdatedBy());
				    ps.setString(8, batch.getCol1Alias1());
				    ps.setString(9, batch.getCol2Alias2());
				    ps.setString(10, batch.getCol3Alias3());
				    ps.setBoolean(11, batch.isEnbl1());
				    ps.setBoolean(12, batch.isEnbl2());
				    ps.setBoolean(13, batch.isEnbl3());
					return ps;
				}
			});
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessLayerException("Error while Inserting Batch Dao", e);
		}
	}
	@Override
	public List<Batch> fetchBatchList(int orgId) throws DataAccessLayerException {
		List<Batch> batchList=null;
		StringBuilder query=new StringBuilder();
		if(orgId==0){
			query.append(" SELECT * FROM `batch` WHERE  `isActive` =1 ");
		}else{
			query.append(" SELECT * FROM `batch` WHERE `orgId` = "+orgId+" AND `isActive` =1 ");
		}
		
		
		try{
			batchList = getJdbcTemplate().query(query.toString(),new RowMapper<Batch>(){

			@Override
			public Batch mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Batch batch = new Batch();
				batch.setId(rs.getInt("id"));
				batch.setBatchName(rs.getString("batchName"));
				batch.setFromTime(rs.getString("fromTime"));
				batch.setToTime(rs.getString("toTime"));
				batch.setOrgId(rs.getInt("orgId"));
				batch.setProfilePic(rs.getString("profilePic"));
				batch.setCol1Alias1(rs.getString("col1_alias1"));
				batch.setCol2Alias2(rs.getString("col2_alias2"));
				batch.setCol3Alias3(rs.getString("col3_alias3"));
				batch.setEnbl1(rs.getBoolean("col1_enbl1"));
				batch.setEnbl2(rs.getBoolean("col2_enbl2"));
				batch.setEnbl3(rs.getBoolean("col2_enbl3"));
				return batch;
				}
			});

		} catch (Exception excp) {
			throw new DataAccessLayerException("Error in accessing the org Details" + excp.getMessage(),excp);
		}
		return batchList;
	}
	@Override
	public List<Batch> fetchBatchDeatilList(int id)
			throws DataAccessLayerException {
		// TODO Auto-generated method stub
		List<Batch> batchList=null;
		StringBuilder query=new StringBuilder();
	
		query.append("SELECT *,org.orgName FROM `batch` AS b ");
		query.append("LEFT JOIN `organization` org ON org.`id`=b.orgId ");
		query.append("LEFT JOIN `user` usr1 ON usr1.`id`=org.`userId` ");
		query.append("WHERE usr1.`id`="+id+"");
		/*if(batchList.getRoleId() == 1){ //for member to do in constant
			query.append("SELECT `batchName`, `fromTime`, `toTime` FROM `batch`");
		}
		else{
			query.append("SELECT `batchName`, `fromTime`, `toTime` FROM `batch` WHERE `id` = "+ id);
		}*/
		try{ 
			batchList = getJdbcTemplate().query(query.toString(),new RowMapper<Batch>(){

			@Override
			public Batch mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Batch batch = new Batch();
				batch.setId(rs.getInt("id"));
				batch.setBatchName(rs.getString("batchName"));
				batch.setFromTime(rs.getString("fromTime"));
				batch.setToTime(rs.getString("toTime"));
				batch.setOrgName(rs.getString("orgName"));
				batch.setOrgId(rs.getInt("orgId"));
				batch.setCol1Alias1(rs.getString("col1_alias1"));
				batch.setCol2Alias2(rs.getString("col2_alias2"));
				batch.setCol3Alias3(rs.getString("col3_alias3"));
				batch.setEnbl1(rs.getBoolean("col1_enbl1"));
				batch.setEnbl2(rs.getBoolean("col2_enbl2"));
				batch.setEnbl3(rs.getBoolean("col2_enbl3"));
				return batch;
				}
			});

		} catch (Exception excp) {
			throw new DataAccessLayerException("Error in accessing the org Details" + excp.getMessage(),excp);
		}
		return batchList;
	}
	@Override
	public Boolean isBatchNameUnique(String batchName, int userId)
			throws DataAccessLayerException {
		// TODO Auto-generated method stub
		final String queryString ="SELECT COUNT(*) AS duplicateCount FROM `batch` b LEFT JOIN `organization` org ON org.id=b.orgId LEFT JOIN `user` usr1 ON usr1.`id`=org.`userId` WHERE `batchName`='"+batchName+"' AND usr1.`id`="+userId+"";
/*		queryString.append("SELECT COUNT(*) AS duplicateCount FROM `batch` b ");
        queryString.append("LEFT JOIN `organization` org ON org.id=b.orgId ");
        queryString.append("LEFT JOIN `user` usr1 ON usr1.`id`=org.`userId`");
        queryString.append("WHERE `batchName`="+batchName+"AND usr1.`id`="+userId);*/
        
		Integer intCntOfAppearance = getJdbcTemplate().queryForInt(queryString);
		if(intCntOfAppearance==0){
			return true;
		}
		else{
			return false;
		}
	}
	@Override
	public void updateBatch(final Batch batch) throws DataAccessLayerException {
		final StringBuilder queryString = new StringBuilder();
	
		queryString.append("UPDATE batch SET `batchName`=?,`fromTime`=?,`toTime`=?, `orgId`=?, `updatedBy`=?, `updatedOn`= NOW(),`col1_alias1`=?, ");
		queryString.append("`col2_alias2`=?,`col3_alias3`=?,`col1_enbl1`=?,`col2_enbl2`=?, `col2_enbl3`=? WHERE id=?");

		try {
			getJdbcTemplate().update(new PreparedStatementCreator(){
				public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{					
					PreparedStatement ps = connection.prepareStatement(queryString.toString());
					ps.setString(1,batch.getBatchName());
					ps.setString(2, batch.getFromTime());
					ps.setString(3, batch.getToTime());
					ps.setInt(4, batch.getOrgId());
				    ps.setInt(5, batch.getUpdatedBy());
				    ps.setString(6, batch.getCol1Alias1());
				    ps.setString(7, batch.getCol2Alias2());
				    ps.setString(8, batch.getCol3Alias3());
				    ps.setBoolean(9, batch.isEnbl1());
				    ps.setBoolean(10, batch.isEnbl2());
				    ps.setBoolean(11, batch.isEnbl3());
				    ps.setInt(12, batch.getId());
				    
					return ps;
				}
			});
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessLayerException("Error while Inserting Batch Dao", e);
		}
		
	}
	@Override
	public List<Batch> fetchBatchListByManagerId(int id)
			throws DataAccessLayerException {
		List<Batch> batchList=null;
		StringBuilder query=new StringBuilder();
		
			query.append("SELECT * FROM batch b LEFT JOIN `user_batch` ub ON ub.`batchId`=b.id WHERE `userId` = "+id+" ");
		
		try{
			batchList = getJdbcTemplate().query(query.toString(),new RowMapper<Batch>(){

			@Override
			public Batch mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Batch batch = new Batch();
				batch.setId(rs.getInt("id"));
				batch.setBatchName(rs.getString("batchName"));
				batch.setFromTime(rs.getString("fromTime"));
				batch.setToTime(rs.getString("toTime"));
				batch.setOrgId(rs.getInt("orgId"));
				batch.setProfilePic(rs.getString("profilePic"));
				batch.setCol1Alias1(rs.getString("col1_alias1"));
				batch.setCol2Alias2(rs.getString("col2_alias2"));
				batch.setCol3Alias3(rs.getString("col3_alias3"));
				batch.setEnbl1(rs.getBoolean("col1_enbl1"));
				batch.setEnbl2(rs.getBoolean("col2_enbl2"));
				batch.setEnbl3(rs.getBoolean("col2_enbl3"));
				return batch;
				}
			});

		} catch (Exception excp) {
			throw new DataAccessLayerException("Error in accessing the org Details" + excp.getMessage(),excp);
		}
	
		return batchList;
	}
	
	
	
	
	/*@Override
	public List<Batch> fetchBatchDeatilList(int id, int roleId)
			throws DataAccessLayerException {
		// TODO Auto-generated method stub
		List<Batch> batchList=null;
		StringBuilder query=new StringBuilder();
		Batch batch= new Batch();
		//query.append("SELECT `batchName`, `fromTime`, `toTime` FROM `batch` WHERE `id` = "+ id);
		query.append("SELECT `batchName`, `fromTime`, `toTime` FROM `batch`");
		if(roleId == 1){ //for member to do in constant
			query.append("SELECT `batchName`, `fromTime`, `toTime` FROM `batch` where ");
		}
		else{
			query.append("SELECT `batchName`, `fromTime`, `toTime` FROM `batch` WHERE `id` = "+ id);
		}
		try{ 
			batchList = getJdbcTemplate().query(query.toString(),new RowMapper<Batch>(){

			@Override
			public Batch mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				Batch batch = new Batch();
				batch.setBatchName(rs.getString("batchName"));
				batch.setFromTime(rs.getString("fromTime"));
				batch.setToTime(rs.getString("toTime"));
				return batch;
				}
			});

		} catch (Exception excp) {
			throw new DataAccessLayerException("Error in accessing the org Details" + excp.getMessage(),excp);
		}
		return batchList;
	}*/
	
	}

