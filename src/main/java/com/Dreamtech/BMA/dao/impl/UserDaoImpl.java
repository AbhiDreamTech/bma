package com.Dreamtech.BMA.dao.impl;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import com.Dreamtech.BMA.bean.Batch;
import com.Dreamtech.BMA.bean.Role;
import com.Dreamtech.BMA.bean.User;
import com.Dreamtech.BMA.dao.AbstractDao;
import com.Dreamtech.BMA.dao.DataAccessLayerException;
import com.Dreamtech.BMA.dao.UserDao;
import com.Dreamtech.BMA.util.Encryptor;


@Repository
public class UserDaoImpl  extends AbstractDao implements UserDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserDaoImpl.class);
	static DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
	static DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
	
	SimpleDateFormat dateFormat2= new SimpleDateFormat("dd-MMM-yyyy");

	@Override
	public User registerUser(final User user)
			throws DataAccessLayerException {
		TransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
		TransactionStatus transactionStatus = getTransactionManager().getTransaction(transactionDefinition);
		final StringBuilder queryString = new StringBuilder();
		KeyHolder keyHolder=new GeneratedKeyHolder();
		queryString.append("INSERT INTO user (`firstname`,`lastname`,`username`,`dob`,`gender`,`email`,`phone`,`isActive`,`createdby`,`createdOn`,`updatedBy`,`updatedOn`, `parentId`) VALUES (?,?,?,?,?,?,?,TRUE,?,NOW(),?,NOW(),?);");

		try {
			getJdbcTemplate().update(new PreparedStatementCreator(){
				public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{					
					PreparedStatement ps = connection.prepareStatement(queryString.toString(),Statement.RETURN_GENERATED_KEYS);
					ps.setString(1,user.getFirstName());
					ps.setString(2,user.getLastName());
					ps.setString(3,user.getUserName());
					
					try {
						if(user.getDOBString()!=null){
						ps.setDate(4, new Date((dateFormat.parse(user.getDOBString())).getTime()));
						}else{ps.setDate(4,null);}
						} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					ps.setString(5,user.getGender());
					ps.setString(6,user.getEmail());
					ps.setString(7,user.getPhoneNo());
					ps.setInt(8,user.getCreatedBy());
					ps.setInt(9,user.getUpdatedBy());
					ps.setInt(10,0);  // for contract parent is will be 0
					
					return ps;
				}
			},keyHolder);
			user.setId(keyHolder.getKey().intValue());
			user.setRoleId(1); // todo
			//updateParentId(user);
			insertUserPassword(user);
			insertUserRole(user);
			getTransactionManager().commit(transactionStatus);
			
			return user;
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			getTransactionManager().rollback(transactionStatus);
			throw new DataAccessLayerException("Error while Inserting user ", e);
		}
		
	}
	
	
	private void insertUserPassword(final User user) throws DataAccessLayerException {
		final StringBuilder queryString = new StringBuilder();
		 queryString.append("INSERT INTO `password` (`userId`,`password`)  VALUES (?,?)");

		try {
			getJdbcTemplate().update(new PreparedStatementCreator(){
				public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{					
					PreparedStatement ps = connection.prepareStatement(queryString.toString());
					
					ps.setInt(1, user.getId());
					ps.setString(2, user.getPassword());
					
					return ps;
				}
			});
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessLayerException("Error while Inserting User service", e);
		}
	}
	
	private void insertUserRole(final User user) throws DataAccessLayerException {
		final StringBuilder queryString = new StringBuilder();
		 queryString.append("INSERT INTO `user_roles` (`userId`,`roleId`,`updatedBy`,`isActive`)  VALUES (?,?,?,TRUE)");

		try {
			getJdbcTemplate().update(new PreparedStatementCreator(){
				public PreparedStatement createPreparedStatement(Connection connection)throws SQLException{					
					PreparedStatement ps = connection.prepareStatement(queryString.toString());
					
					ps.setInt(1, user.getId());
					ps.setInt(2, user.getRoleId()); //contaractor todo in constant
					ps.setInt(3, user.getUpdatedBy());
					return ps;
				}
			});
		} catch(Exception e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessLayerException("Error while Inserting User service", e);
		}
	}
		
	@Override
	public User loginUser(User user) throws DataAccessLayerException {
		User loggedInUserDetails =null;
		StringBuilder query=new StringBuilder();
		query.append("SELECT u.* ,rl.`roleName`,rl.`id` AS roleId,org.`orgName`,org.`id` as orgId,batch.`id` as batchId,batch.batchName as batchname, ");
		/*query.append(" (SELECT `value` FROM `user_local` WHERE userId=u.`id` AND appDefaultId=1) AS organizationAlias, ");
		query.append(" (SELECT `value` FROM `user_local` WHERE userId=u.`id` AND appDefaultId=2) AS batchAlias, ");
		query.append(" (SELECT `value` FROM `user_local` WHERE userId=u.`id` AND appDefaultId=3) AS managerAlias ");;*/
		
		         query.append("  (CASE WHEN urole.`roleId`=1 THEN ");
				 query.append(" (SELECT `value` FROM `user_local` WHERE userId=u.`id` AND appDefaultId=1) ");
		         query.append("  ELSE ");
				 query.append(" (SELECT `value` FROM `user_local` WHERE userId=u.`parentId` AND appDefaultId=1) ");
				 query.append(" END) AS organizationAlias, ");
				    
			     query.append("	(CASE WHEN urole.`roleId`=1 THEN ");
				 query.append(" (SELECT `value` FROM `user_local` WHERE userId=u.`id` AND appDefaultId=2) ");
				 query.append("   ELSE ");
				 query.append("  (SELECT `value` FROM `user_local` WHERE userId=u.`parentId` AND appDefaultId=2) ");
				 query.append("  END) AS batchAlias, ");
				    
				 query.append("	(CASE WHEN urole.`roleId`=1 THEN ");
				 query.append("   (SELECT `value` FROM `user_local` WHERE userId=u.`id` AND appDefaultId=3) ");
				 query.append("  ELSE ");
				 query.append("   (SELECT `value` FROM `user_local` WHERE userId=u.`parentId` AND appDefaultId=3) ");
				 query.append("  END) AS managerAlias ");
		
		query.append(" FROM `user` u ");
		query.append(" LEFT JOIN `password` p  ON p.`userId`=u.`id` ");
		query.append(" LEFT JOIN`user_roles` urole  ON urole.`userId`=u.`id` ");
		query.append(" LEFT JOIN `role` rl ON rl.`id`=urole.`roleId` ");
		
		query.append(" LEFT JOIN `user_batch` ubatch ON ubatch.`userId`=u.`id` ");
		query.append(" LEFT JOIN `batch` batch ON batch.`id`=ubatch.`batchId` ");
		query.append(" LEFT JOIN `organization` org ON org.`id`=batch.`orgId` ");
		
		query.append(" WHERE u.username = ? AND aes_decrypt(unhex(p.`password`),?)= ?  LIMIT 1 ");
		System.out.println(query);
		try{
			loggedInUserDetails = getJdbcTemplate().queryForObject(query.toString(), new Object[]{user.getUserName(), Encryptor.key,user.getPassword()}, new RowMapper<User>(){
				@Override
				public User mapRow(ResultSet rs, int rowNum) throws SQLException {
					User user = new User();
					user.setId(rs.getInt("id"));
					user.setFirstName(rs.getString("firstname"));
					user.setLastName(rs.getString("lastname"));
					user.setUserName(rs.getString("username"));
					user.setGender(rs.getString("gender"));
					user.setEmail(rs.getString("email"));
					user.setPhoneNo(rs.getString("phone"));
					user.setActive(rs.getBoolean("isActive"));
					user.setProfilePic(rs.getString("profilePic"));
					user.setCreatedBy(rs.getInt("createdby"));
					user.setUpdatedBy(rs.getInt("updatedBy"));
					user.setParentId(rs.getInt("parentId"));
					if(rs.getDate("dob") !=null){
						try {
							user.setDOB(dateFormat1.parse(rs.getString("dob")));
							} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							}
						user.setDOBString(dateFormat2.format(user.getDOB()));
					}
					user.setMedicalHistory(rs.getString("medical_history"));
					user.setOrgName(rs.getString("orgName"));
					user.setOrgId(rs.getInt("orgId"));
					user.setBatchName(rs.getString("batchName"));
					user.setBatchId(rs.getInt("batchId"));
					user.setOrganizationAlias(rs.getString("organizationAlias"));
					user.setBatchAlias(rs.getString("batchAlias"));
					user.setManagerAlias(rs.getString("managerAlias"));
					Role role = new Role();
					role.setId(rs.getInt("roleId"));
					role.setName(rs.getString("roleName"));
					
					user.setRoleObj(role);
					return user;
					
				}});
		}
		catch (EmptyResultDataAccessException emptyResultDataAccessException) {
			
			LOGGER.error("No such user exists with username : " + user.getUserName(), emptyResultDataAccessException);
			throw new DataAccessLayerException("No user exists with username : " + user.getUserName(), emptyResultDataAccessException);
		} catch (Exception excp) {	
			excp.printStackTrace();
			LOGGER.error("Error fetching user data with username : " + user.getUserName(), excp);			
			throw new DataAccessLayerException("Error fetching user data with username : " + user.getUserName(), excp);
		}
		return loggedInUserDetails;
	}
	

	
}
