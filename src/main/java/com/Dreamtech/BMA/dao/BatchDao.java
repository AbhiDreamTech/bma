package com.Dreamtech.BMA.dao;

import java.util.List;

import com.Dreamtech.BMA.bean.Batch;
import com.Dreamtech.BMA.service.ApplicationException;

public interface BatchDao {
	
	void addBatch(Batch batch) throws DataAccessLayerException;

	List<Batch> fetchBatchList(int orgId) throws DataAccessLayerException;
	
	List<Batch> fetchBatchDeatilList(int id) throws DataAccessLayerException;
	
	Boolean isBatchNameUnique(String batchName, int userId) throws DataAccessLayerException;

	void updateBatch(Batch batch)throws DataAccessLayerException;

	List<Batch> fetchBatchListByManagerId(int id) throws DataAccessLayerException;
	
	//List<Batch> fetchBatchDeatilList(int id, int roleId)throws DataAccessLayerException;
}
