package com.Dreamtech.BMA.dao;

import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.transaction.PlatformTransactionManager;



/**
 * Base class for managing data entity instances.
 * Contains common CRUD methods.
 *
 * @param <E> class of the entity that subclasses will manage
 * @param <I> class of the identity of the managed entity
 */
public class AbstractDao  extends JdbcDaoSupport {
	protected PlatformTransactionManager transactionManager;

	public PlatformTransactionManager getTransactionManager() {
		return transactionManager;
	}

	public void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}
	


}
