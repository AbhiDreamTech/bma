package com.Dreamtech.BMA.dao;

import java.text.ParseException;
import java.util.List;

import com.Dreamtech.BMA.bean.Role;
import com.Dreamtech.BMA.bean.User;

public interface UserDao {

	User registerUser(User user) throws DataAccessLayerException;
	
	User loginUser(User user) throws DataAccessLayerException;
	
	
	
}