package com.Dreamtech.BMA.bean;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;

@Component
@XmlRootElement
public class User extends AbstractBean{

	private Integer id;
	private Integer memberId;
	private String userName;
	private String password;
	private String email;
	private String firstName;
	private String lastName;
	private Date  DOB;
	private String  attendanceDate;
	private Boolean present;
	private String orgName;
	private String batchName;
	private String DOBString;
	private String gender;
	private String phoneNo;
	private Date  DOJoining;
	private String profilePic;
	private Integer parentId;
	private Boolean isAssigned;
	private Integer batchId;
	private Integer userId;
	private Integer roleId;
	private Integer userRoleId;
	private Integer prevMngrId;
	private String col1;
	private String col2;
	private String col3;
	private String menuName;
	private String urlMap;
	private String organizationAlias;
	private String batchAlias;
	private String managerAlias;
	private String emailContent;
	private String  attendanceEndDate;
	private String medicalHistory;
	private Integer	orgId;
	private Role roleObj;
	private Batch userBatch;
	private Boolean duplicateCount;
	private byte[] uploadfile;
	private String imagePath;
	private String appversion;
    
	private String otp;
	private String dueDate;
	private String dueDateStr;
    private int countofpresent;
    private int totalAttn;
    private int due;
    private int totalMemb;
    private int diff;
	private Date dueDateD;
	private List<Batch> batchList;
	
	public int getDiff() {
		return diff;
	}

	public void setDiff(int diff) {
		this.diff = diff;
	}

	public int getDue() {
		return due;
	}

	public void setDue(int due) {
		this.due = due;
	}

	public int getCountofpresent() {
		return countofpresent;
	}

	public void setCountofpresent(int countofpresent) {
		this.countofpresent = countofpresent;
	}

	public int getTotalAttn() {
		return totalAttn;
	}

	public void setTotalAttn(int totalAttn) {
		this.totalAttn = totalAttn;
	}
	public String getDueDateStr() {
		return dueDateStr;
	}

	public void setDueDateStr(String dueDateStr) {
		this.dueDateStr = dueDateStr;
	}

	public String getEmailContent() {
		return emailContent;
	}

	public void setEmailContent(String emailContent) {
		this.emailContent = emailContent;
	}
	
	public String getOtp() {
		return otp;
	}

	public void setOtp(String otp) {
		this.otp = otp;
	}

	public void setFile(byte[] uploadfile) {
        this.uploadfile = uploadfile;
    }

    public byte[] getFile() {
        return uploadfile;
    }
    
	public Boolean getDuplicateCount() {
		return duplicateCount;
	}
	public void setDuplicateCount(Boolean duplicateCount) {
		this.duplicateCount = duplicateCount;
	}
	private List<User> userList;
	
	public List<User> getUserList() {
		return userList;
	}
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Date getDOB() {
		return DOB;
	}
	public void setDOB(Date dOB) {
		DOB = dOB;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public Date getDOJoining() {
		return DOJoining;
	}
	public void setDOJoining(Date dOJoining) {
		DOJoining = dOJoining;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	
	public String setOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		
		this.orgName = orgName;
	}
	
	public Integer setOrgId() {
		return orgId;
	}
	public void setOrgId(Integer orgId) {
		
		this.orgId = orgId;
	}
	
	public String getDOBString() {
		return DOBString;
	}
	public void setDOBString(String dOBString) {
		DOBString = dOBString;
		
	}
	public Boolean getIsAssigned() {
		return isAssigned;
	}
	public void setIsAssigned(Boolean isAssigned) {
		this.isAssigned = isAssigned;
	}
	public Integer getBatchId() {
		return batchId;
	}
	public void setBatchId(Integer batchId) {
		this.batchId = batchId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public Role getRoleObj() {
		return roleObj;
	}
	public void setRoleObj(Role roleObj) {
		this.roleObj = roleObj;
	}
	public Batch getUserBatch() {
		return userBatch;
	}
	public void setUserBatch(Batch userBatch) {
		this.userBatch = userBatch;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	
	public Integer getUserRoleId() {
		return userRoleId;
	}
	public void setUserRoleId(Integer userRoleId) {
		this.userRoleId = userRoleId;
	}  
	public Integer getPrevMngrId() {
		return prevMngrId;
	}
	public void setPrevMngrId(Integer prevMngrId) {
		this.prevMngrId = prevMngrId;
	}
	
	public String getAttendanceDate() {
		return attendanceDate;
	}
	public void setAttendanceDate(String attendanceDate) {
		this.attendanceDate = attendanceDate;
		
	}
		
	public Boolean getPresent() {
		return present;
	}
	public void setPresent(Boolean present) {
		this.present = present;
	}
	
	public String setBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		
		this.batchName = batchName;
	}
	
	public Integer getMemberId() {
		return memberId;
	}
	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}
	
	public String getCol1() {
		return col1;
	}
	public void setCol1(String col1) {
		this.col1 = col1;
	}
	public String getCol2() {
		return col2;
	}
	public void setCol2(String col2) {
		this.col2 = col2;
	}
	public String getCol3() {
		return col3;
	}
	public void setCol3(String col3) {
		this.col3 = col3;
	}
	
	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	
	public String getMenuName() {
		return menuName;
	}

	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}

	public String getUrlMap() {
		return urlMap;
	}

	public void setUrlMap(String urlMap) {
		this.urlMap = urlMap;
	}

	public String getOrganizationAlias() {
		return organizationAlias;
	}

	public void setOrganizationAlias(String organizationAlias) {
		this.organizationAlias = organizationAlias;
	}

	public String getBatchAlias() {
		return batchAlias;
	}

	public void setBatchAlias(String batchAlias) {
		this.batchAlias = batchAlias;
	}

	public String getManagerAlias() {
		return managerAlias;
	}

	public void setManagerAlias(String managerAlias) {
		this.managerAlias = managerAlias;
	}

	public String getAttendanceEndDate() {
		return attendanceEndDate;
	}

	public void setAttendanceEndDate(String attendanceEndDate) {
		this.attendanceEndDate = attendanceEndDate;
	}

	public String getMedicalHistory() {
		return medicalHistory;
	}

	public void setMedicalHistory(String medicalHistory) {
		this.medicalHistory = medicalHistory;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public Date getDueDateD() {
		return dueDateD;
	}

	public void setDueDateD(Date dueDateD) {
		this.dueDateD = dueDateD;
	}

	public int getTotalMemb() {
		return totalMemb;
	}

	public void setTotalMemb(int totalMemb) {
		this.totalMemb = totalMemb;
	}

	public String getAppversion() {
		return appversion;
	}

	public void setAppversion(String appversion) {
		this.appversion = appversion;
	}

	public List<Batch> getBatchList() {
		return batchList;
	}

	public void setBatchList(List<Batch> batchList) {
		this.batchList = batchList;
	}
	
	
}
