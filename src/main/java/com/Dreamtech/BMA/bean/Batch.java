package com.Dreamtech.BMA.bean;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.stereotype.Component;


@Component
@XmlRootElement
public class Batch  extends AbstractBean{
	
	private int id;
	private String batchName;
	private String fromTime;
	private String toTime;
	private int orgId;
	private String orgName;
	private String profilePic;
	private String col1Alias1;
	private String col2Alias2;
	private String col3Alias3;
	
	private boolean enbl1;
	private boolean enbl2;
	private boolean enbl3;
	
	public boolean isEnbl2() {
		return enbl2;
	}
	public void setEnbl2(boolean enbl2) {
		this.enbl2 = enbl2;
	}
	public boolean isEnbl3() {
		return enbl3;
	}
	public void setEnbl3(boolean enbl3) {
		this.enbl3 = enbl3;
	}
	public String getBatchName() {
		return batchName;
	}
	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}
	public String getFromTime() {
		return fromTime;
	}
	public void setFromTime(String fromTime) {
		this.fromTime = fromTime;
	}
	
	public String getCol1Alias1() {
		return col1Alias1;
	}
	public void setCol1Alias1(String col1Alias1) {
		this.col1Alias1 = col1Alias1;
	}
	public String getCol2Alias2() {
		return col2Alias2;
	}
	public void setCol2Alias2(String col2Alias2) {
		this.col2Alias2 = col2Alias2;
	}
	public String getCol3Alias3() {
		return col3Alias3;
	}
	public void setCol3Alias3(String col3Alias3) {
		this.col3Alias3 = col3Alias3;
	}
	public boolean isEnbl1() {
		return enbl1;
	}
	public void setEnbl1(boolean enbl1) {
		this.enbl1 = enbl1;
	}
	public int getOrgId() {
		return orgId;
	}
	public void setOrgId(int orgId) {
		this.orgId = orgId;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public String getToTime() {
		return toTime;
	}
	public void setToTime(String toTime) {
		this.toTime = toTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getOrgName() {
		return orgName;
	}
	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

}
