package com.Dreamtech.BMA.bean;

import java.util.Date;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class AbstractBean {
	
	private boolean isActive;
	private Date createdDate;
	private Integer createdBy;
	private Date updatedDate;
	private Integer updatedBy;

	
	
	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public static Gson getGson() {
		return GSON;
	}

	private static final Gson GSON = new GsonBuilder().setPrettyPrinting().create();
	
	@Override
	public String toString() {
		return GSON.toJson(this);
	}
	
}
